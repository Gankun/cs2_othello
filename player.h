#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include "common.h"
#include "board.h"

using namespace std;

class Player {

public:
    Player(Side side);
    ~Player();
    
    Move *doMove(Move *opponentsMove, int msLeft);
	
    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;
    void setBoard(Board *map);
private:
	class Board *map;
	Side me;
	int page;
	Side enemy;
	Move *Openning(Move* opponentsMove, Side side, int page);
	
};

#endif
