#include "player.h"
// Matthew Cedeno
// 3/9/2014

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */

Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;
	map = new Board();
	page = 0;
	me = side;
	enemy = (side == BLACK) ? WHITE : BLACK;
    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
}

/*
 * Destructor for the player.
 */
Player::~Player() {
	delete map;
	
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
    /* 
     * TODO: Implement how moves your AI should play here. You should first
     * process the opponent's opponents move before calculating your own move
     */ 
     map->doMove(opponentsMove, enemy);
	 int CODE = 0;
     
     if (map->isDone()) return NULL;
     if (map->hasMoves(me) == false) return NULL;
   
	/* if only one move available, do it, save time.
		map->get_moves(me);
		vector<Move *> my_choices(map->getMoveVec());
		if (my_choices.size() == 1)
		{
			return map->bestMove(me);
		}
   */
     if (testingMinimax)
     {
		 // use minimax for testing purposes
		 Move * myturn;
		 pair <Move*, int> helix;
		 helix = map->level2(me);
		 myturn = helix.first;
		 map->doMove(myturn, me);
	     return myturn;
	 }   
	 // follow opening book as long as possible.
	 Move* myturnbook;
	 myturnbook = Openning(opponentsMove, me, page);
	 if (myturnbook != NULL)
	 {
		 page++;
		 if (map->checkMove(myturnbook, me))
		 {
			map->doMove(myturnbook, me);
			return myturnbook;
		}
		// case where deviation from turn book
		else
		{
			page = -1;
		}
	 }

	 
	 
	 // Gameplan for 6 depth minmax. Code 0
	 if (CODE == 0)
	 {
		Move * myturn2;
		pair <Move*, int> helix2; 
		helix2 = map->recursive_MinMaxV2(me, me, 5);
		myturn2 = helix2.first;
		// always double check moves, if error use best move
		if (map->checkMove(myturn2, me))
		{
			map->doMove(myturn2, me);
		}
		else
		{
			myturn2 = map->bestMove(me);
			map->doMove(myturn2, me);
		}
		return myturn2;
	}
	 // Gameplan for 4 depth minmax. Code 1
	 if (CODE == 1){
		Move * helix3;
	 
		helix3 = map->level4_2(me);
		map->doMove(helix3, me);
		return helix3;}
	 
	 
	 
	 // GamePlan using 2 depth minmax Code 2
	 if (CODE == 2)
	 {
	 Move * myturn2;
	 pair <Move*, int> helix2; 
	 myturn2 = map->level4(me);
	 map->doMove(myturn2, me);
	 return myturn2;
	}
	// Beating Random player
     Move * myturn;
     map->get_moves(me);
    
	 myturn = map->bestMove(me);
	 map->doMove(myturn, me);
	 return myturn;
    
}
void Player::setBoard(Board *new_board){
	map = new_board;
}
/*
 * Oppening moves for my player. Bases on which side it starts from
 * returns NULL when time to break from book.
 */
Move *Player::Openning(Move* opponentsMove, Side side, int page){
	if (side == BLACK)
	{	
		if (page == 0)
		{
			Move * book = new Move(2,3);
			return book;
		}
		else if(page == 1 && opponentsMove->getX() == 2 && opponentsMove->getY() == 2)
		{
			Move * book = new Move(4,5);
			return book;
		}
		else if(page == 1 && opponentsMove->getX() == 2 && opponentsMove->getY() == 4)
		{
			Move * book = new Move(2,5);
			return book;
		}
		else if(page == 2 && opponentsMove->getX() == 2 && opponentsMove->getY() == 4)
		{
			Move * book = new Move(3,2);
			return book;
		}
		else if(page == 2 && opponentsMove->getX() == 1 && opponentsMove->getY() == 4)
		{
			Move * book = new Move(0,5);
			return book;
		}
		else if(page == 3 && opponentsMove->getX() == 4 && opponentsMove->getY() == 2)
		{
			Move * book = new Move(4,1);
			return book;
		}
		else if(page == 3 && opponentsMove->getX() == 0 && opponentsMove->getY() == 4)
		{
			Move * book = new Move(0,3);
			return book;
		}
		else if(page == 4 && opponentsMove->getX() == 5 && opponentsMove->getY() == 0)
		{
			Move * book = new Move(1,5);
			return book;
		}
		//else if(page == 4 && opponentsMove->getX() == 2 && opponentsMove->getY() == 2)
	//	{
		//	Move * book = new Move(2,1);
		//	return book;
	//	}
		
		else
		{
			return NULL;
		}
	}
	else
	{
		// white case
		if (page == 0 && opponentsMove->getX() == 2 && opponentsMove->getY() == 3)
		{
			Move * book = new Move(2,2);
			return book;
		}
		else if(page == 1 && opponentsMove->getX() == 2 && opponentsMove->getY() == 1)
		{
			Move * book = new Move(3,5);
			return book;
		}
		else if(page == 2 && opponentsMove->getX() == 5 && opponentsMove->getY() == 5)
		{
			Move * book = new Move(3,2);
			return book;
		}
		
		else
		{
			return NULL;
		}
	}


}
