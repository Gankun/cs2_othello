First I played around with the player file to get a working AI.
Once I grasped the basics of how to get an AI to finish a game and
pick moves based on some rules, I kept improving the rules.
I over time developed a recursive Minmax that could take in the number
of levels I needed it for. I'm going to use 5 layers for my particular
function as it isn't as time consuming and memory demanding as higher layers.
I developed an openning book on some common first moves for both sides that
I felt could lead my AI in the right direction for taking the objectives.
I also assigned values to each spot, increasing the value of taking a 
corner as well as decreasing heavily moves that permited the opponents AI
to potentially grab corners or pieces. I think my openning book will help 
defeat an AI that makes a mistake early on, as well as the custom values.
In my games with betterplayer there were some circumstances where I could
win all at the end, by holding off certain key pieces until the end and 
force the opponents to make a bad move.