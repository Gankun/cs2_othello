#ifndef __BOARD_H__
#define __BOARD_H__

#include <bitset>
#include <vector>
#include "common.h"
#include <utility>
using namespace std;
#define CORNERSCORE 15
#define ENDSCORE -1
#define CROSSCORNER -7
#define NEARCORNER -2
#define NEARENDSCORE 0

class Board {
   
private:
    bitset<64> black;
    bitset<64> taken;    
    
    bool occupied(int x, int y);
    bool get(Side side, int x, int y);
    void set(Side side, int x, int y);
    bool onBoard(int x, int y);
   vector<Move *> available;
    
      
public:
    Board();
    ~Board();
    Board *copy();
    int getNumMoves();
    int getScore(Side score_side);
    int getScore_weighted(Move * move, Side move_side, Side score_side, Board * board);
    int mobility();
    bool isDone();
    bool stable_move(Move * move, Side side);
    void get_moves(Side side);
    bool hasMoves(Side side);
    Move* bestMove(Side side);
    bool checkMove(Move *m, Side side);
    void doMove(Move *m, Side side);
    vector<Move *> getMoveVec(Side side);
    int count(Side side);
    int countBlack();
    int countWhite();
	int getValue(Move *m, Side side);
    void setBoard(char data[]);
    // minimax functions
    pair<Move*, int> level2(Side side);
    pair<Move*, int> level2_score(Side side);
    pair<Move*, int> recursive_MinMax(Side side, int n);
    pair<Move*, int> recursive_MinMaxV2(Side side, Side myself, int n);
    Move* level4(Side side);
    Move* level4_2(Side side);
};

#endif
