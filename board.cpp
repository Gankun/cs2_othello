#include "board.h"

/*
 * Make a standard 8x8 othello board and initialize it to the standard setup.
 */
Board::Board() {
    taken.set(3 + 8 * 3);
    taken.set(3 + 8 * 4);
    taken.set(4 + 8 * 3);
    taken.set(4 + 8 * 4);
    black.set(4 + 8 * 3);
    black.set(3 + 8 * 4);
	
}

/*
 * Destructor for the board.
 */
Board::~Board() {
}

/*
 * Returns a copy of this board.
 */
Board *Board::copy() {
    Board *newBoard = new Board();
    newBoard->black = black;
    newBoard->taken = taken;
    return newBoard;
}

bool Board::occupied(int x, int y) {
    return taken[x + 8*y];
}

bool Board::get(Side side, int x, int y) {
    return occupied(x, y) && (black[x + 8*y] == (side == BLACK));
}


void Board::set(Side side, int x, int y) {
    taken.set(x + 8*y);
    black.set(x + 8*y, side == BLACK);
}

bool Board::onBoard(int x, int y) {
    return(0 <= x && x < 8 && 0 <= y && y < 8);
}

 
/*
 * Returns true if the game is finished; false otherwise. The game is finished 
 * if neither side has a legal move.
 */
bool Board::isDone() {
    return !(hasMoves(BLACK) || hasMoves(WHITE));
}



/*
 * Returns true if there are legal moves for the given side.
 */
bool Board::hasMoves(Side side) {
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            Move move(i, j);
            if (checkMove(&move, side)) return true;
        }
    }
    return false;
}
/*
 * Returns the best move based on looking down Minmax for two levels
 * */
 pair<Move*, int> Board::level2(Side side)
{
	// case where side has no more moves to make.
	if (hasMoves(side) == false) {
		pair<Move*, int> helix (NULL, -64);
		return helix;
	}
	
	Side enemy = (side == BLACK) ? WHITE : BLACK;
	vector<Move *> my_choices(getMoveVec(side));
	vector<Move *> op1;
	// gets possible moves for opponent
	 Move * l2max;
	 //Move * l2min;
	 int max = -64;
	 Board * tempB;
	 // look at possiblilities from my moves
	 for (unsigned i = 0; i < my_choices.size(); i++)
	 {
		 tempB = copy();
		 tempB->doMove(my_choices[i], side);
		 
		 //check if opponent has any possible moves
			
			 op1 = tempB->getMoveVec(enemy);
			 int min = 64;
			 // analyze all enemy moves
			 for (unsigned j = 0; j < op1.size(); j++)
			 {
				 Board * tempC;
				 tempC = tempB->copy();
				 tempC->doMove(op1[j], enemy);
				 int temp_score = tempC->getScore(side);
				 if (temp_score < min)
				 {
					 min = temp_score;
					 //l2min = op1[j];
				 }
			 }
			 if(op1.size() == 0)
			 {
				 // case to count score for no valid moves
				 min = tempB->getScore(side);
			 }
	
		if (min > max)
		 {
			 // the highest of the min
			 max = min;
			 l2max = my_choices[i];
		 }
		 
	} 
		
	 pair <Move *, int> helix(l2max, max);
	 
	 return helix;

}
/*
 * Returns the best move based on looking down Minmax for n amount of
 * personal moves. ex) 1 looks 2 down, 2 looks 4 down, etc.
 * */
 pair<Move*, int> Board::recursive_MinMax(Side side, int n)
{
		// case where side has no more moves to make.
		if (hasMoves(side) == false) {
			pair<Move*, int> helix (NULL, getScore(side));
			return helix;
		}
		get_moves(side);
		Side enemy = (side == BLACK) ? WHITE : BLACK;
		vector<Move *> my_choices(getMoveVec(side));
		vector<Move *> op1;
		 // gets possible moves for opponent
		 // store the best move so far
		 Move * l2max;
		 // assign low score to beat
		 int max = -2000;
		 Board * tempB;
		 // look at possiblilities from my moves
		 for (unsigned i = 0; i < my_choices.size(); i++)
		 {
			 tempB = copy();
			 tempB->doMove(my_choices[i], side);
			 
			 int min;
			
			// immediately assigns corner moves very high scores.
			if (getValue(my_choices[i], side) == CORNERSCORE)
			{
				
				pair <Move *, int> corner(my_choices[i], CORNERSCORE);
				return corner;
			}
			// case to count score for no valid moves for opponent
			else if (tempB->hasMoves(enemy) == false) 
			{
				 min = tempB->getScore(side);
			}
			else{
				 tempB->get_moves(enemy);
				 op1 = tempB->getMoveVec(enemy);
				 
				 //assign high starting min
				 min = 1000;
				 // analyze all enemy moves
				 for (unsigned j = 0; j < op1.size(); j++)
				 {
					 Board * tempC;
					 tempC = tempB->copy();
					 tempC-> doMove(op1[j], enemy);
					 int temp_score = 0;
					 
					 // base case
					 if (n == 1)
					 {
						 temp_score += tempC->getScore(side) + getValue(my_choices[i], side);
										//+ my_choices.size() - op1.size();
					 }
					 else
					 {
						 pair <Move*, int> helix;
						helix = tempC->recursive_MinMax(side, n - 1);
						temp_score += helix.second + getValue(my_choices[i], side);
									//+ my_choices.size() - op1.size();
					
					 }
					 // new min for my move
					 if (temp_score < min)
					 {
						 min = temp_score;
					 }
					delete tempC;
				 }
					 
				// highest min of opponents moves
				if (min > max)
				 {
					 // the highest of the min
					 max = min;
					 l2max = my_choices[i];
				 }
				 delete tempB;
			}
		} 
		 
		 pair <Move *, int> helix(l2max, max);
		 if (l2max == NULL)
		 {
			 //pair <Move *, int> beta(bestMove(side), max);
			 pair <Move *, int> beta(my_choices[0], max);
			 return beta;
		 }
		 return helix;
}/*
 * Returns the best move based on looking down Minmax for n amount of
 * turns 
 * */
 pair<Move*, int> Board::recursive_MinMaxV2(Side side, Side myself, int n)
{
		// case where side has no more moves to make.
		if (hasMoves(side) == false) {
			pair<Move*, int> helix (NULL, getScore(side));
			return helix;
		}
		Side enemy = (side == BLACK) ? WHITE : BLACK;
		vector<Move *> moves(getMoveVec(side));
		 Move * best_move;
		
		 Board * tempB;
		 // set a low max to try and beat.
		 int max = -200;
		 int multiplier = -1;
		 // create a multiplier to apply to moves based on whos move.
		 if (side == myself)
		 {
			 multiplier = 1;
		 }
		 // look at possiblilities from current moves
		 for (unsigned i = 0; i < moves.size(); i++)
		 {
			 // immediately assign a corner move a very high / or low
			 // score, helps to save time.
			 
			if (getValue(moves[i], side) == CORNERSCORE)
			{
				
				pair <Move *, int> corner(moves[i], (multiplier * CORNERSCORE));
				return corner;
			}
			 tempB = copy();
			 tempB->doMove(moves[i], side);
			 int move_score;
			 // base case for last level
			 if (n == 1)
			 {
				 move_score = tempB->getScore(side) + multiplier * getValue(moves[i], side);
			 }
			 // case for n != 1, recursive case
			 else
			 {
				pair <Move*, int> helix;
				helix = tempB->recursive_MinMaxV2(enemy, myself, n - 1);
				move_score = helix.second + multiplier * getValue(moves[i], side);
			
			 }
			 // compare the move scores to the current max. 
			 if (max < move_score){
				 best_move = moves[i];
				 max = move_score;
			 }
			 delete tempB;
		}
		pair <Move *, int> helix(best_move, max);
		return helix;
			
}
/*
 * Returns the best move based on looking down Minmax for two levels
 * takes into account value of pieces
 * */
 pair<Move*, int> Board::level2_score(Side side)
{
	// case where side has no more moves to make.
	if (hasMoves(side) == false) {
		pair<Move*, int> helix (NULL, -64);
		return helix;
	}
	get_moves(side);
	Side enemy = (side == BLACK) ? WHITE : BLACK;
	vector<Move *> my_choices(available);
	vector<Move *> op1;
	// gets possible moves for opponent
	 Move * l2max;
	 //Move * l2min;
	 int max = -64;
	 Board * tempB;
	 // look at possiblilities from my moves
	 for (unsigned i = 0; i < my_choices.size(); i++)
	 {
		 tempB = copy();
		 tempB->doMove(my_choices[i], side);
		 tempB->get_moves(enemy);
		 //check if opponent has any possible moves
			
			 op1 = tempB->getMoveVec(enemy);
			 int min = 64;
			 // analyze all enemy moves
			 for (unsigned j = 0; j < op1.size(); j++)
			 {
				 int temp_score = tempB->getValue(op1[j], enemy) 
									- tempB->getValue(my_choices[i], side);
				 if (temp_score < min)
				 {
					 min = temp_score;
					 //l2min = op1[j];
				 }
			 }
			 if(op1.size() == 0)
			 {
				 // case to count score for no valid moves
				 min = tempB->getScore(side);
			 }
	
		if (min > max)
		 {
			 // the highest of the min
			 max = min;
			 l2max = my_choices[i];
		 }
		 
	} 
		
	 pair <Move *, int> helix(l2max, max);
	 return helix;

}
/*
 * Returns the best move based on looking down Minmax for four levels
 * */
 Move* Board::level4(Side side)
{
	// case where side has no more moves to make.
	if (hasMoves(side) == false) {
		return NULL;
		
	}
	get_moves(side);
	vector<Move *> my_choices(available);
	vector<Move *> op1;
	Side flareon = (side == BLACK) ? WHITE : BLACK; // opposite player
	 Move * l2max;
	 int max = -64;
	 Board * tempC;
	 Board * tempB;
	 // if only one move left, return that move.
	 if(my_choices.size() == 1)
	 {
		 return my_choices[0];
	 }
	 // look at all of my first choices
	 for (unsigned i = 0; i < my_choices.size(); i++)
	 {
		 if (getValue(my_choices[i], side) < 0)
		 {
			 // for bad valued moves, give them a hefty penalty and
			 // save time calculating moves.
			 int bad_score = getValue(my_choices[i], side) * -6;
				if (bad_score> max)
				 {
					 max = bad_score;
					 l2max = my_choices[i];
				 }
		 }
		 else
		 {
			 // simulate my move
			 tempB = copy();
			 tempB->doMove(my_choices[i], side);
			 tempB->get_moves(flareon);
			 op1 = tempB->getMoveVec(flareon);
			 // if a move stalls opponent, calculate the score.
			 if(tempB->hasMoves(flareon) == false)
			{
				int stall_score = tempB->getScore(side);
				if (stall_score> max)
				 {
					 max = stall_score;
					 l2max = my_choices[i];
				 }
			}
			 // look at all of enemy's options
			 for (unsigned j = 0; j < op1.size(); j++)
			 {
				 tempC = tempB->copy();
				 tempC->doMove(op1[j], flareon);
				 pair <Move*, int> helix;
				 helix = tempC->level2(side);
				 if (helix.second > max)
				 {
					 max = helix.second;
					 l2max = my_choices[i];
				 }
			 }
		 }

	 }
	 return l2max;
			 
}/*
 * Returns the best move based on looking down Minmax for four levels
 * using a weighted score.
 * */
 Move* Board::level4_2(Side side)
{
	// case where side has no more moves to make.
	if (hasMoves(side) == false) {
		return NULL;
		
	}
	get_moves(side);
	vector<Move *> my_choices(available);
	vector<Move *> op1;
	Side flareon = (side == BLACK) ? WHITE : BLACK; // opposite player
	 Move * l2max;
	 int max = -64;
	 Board * tempC;
	 Board * tempB;
	 // if only one move left, return that move.
	 if(my_choices.size() == 1)
	 {
		 return my_choices[0];
	 }
	 // look at all of my first choices
	 for (unsigned i = 0; i < my_choices.size(); i++)
	 {
		 if (getValue(my_choices[i], side) < 0)
		 {
			 // for bad valued moves, give them a hefty penalty and
			 // save time calculating moves.
			 int bad_score = getValue(my_choices[i], side) * -6;
				if (bad_score> max)
				 {
					 max = bad_score;
					 l2max = my_choices[i];
				 }
		 }
		 else
		 {
			 // simulate my move
			 tempB = copy();
			 tempB->doMove(my_choices[i], side);
			 tempB->get_moves(flareon);
			 op1 = tempB->getMoveVec(flareon);
			 // if a move stalls opponent, calculate the score.
			 if(tempB->hasMoves(flareon) == false)
			{
				int stall_score = tempB->getScore(side);
				if (stall_score> max)
				 {
					 max = stall_score;
					 l2max = my_choices[i];
				 }
			}
			 // look at all of enemy's options
			 for (unsigned j = 0; j < op1.size(); j++)
			 {
				 tempC = tempB->copy();
				 tempC->doMove(op1[j], flareon);
				 pair <Move*, int> helix;
				 helix = tempC->level2_score(side);
				 if (helix.second > max)
				 {
					 max = helix.second;
					 l2max = my_choices[i];
				 }
			 }
		 }

	 }
	 return l2max;
			 
}
/*
 * Returns the vector containing all the available moves 
 * 
 */
 vector<Move *> Board::getMoveVec(Side side)
 {
	 get_moves(side);
	 return available;
 }
 /*
 * Returns the number of all the available moves 
 * 
 */
 int Board::getNumMoves()
 {
	 return available.size();
 }
/*
 * returns the moves with the highest value of all the moves in 
 * available. 
 */
 Move* Board::bestMove(Side side)
 { 
  get_moves(side);
  int score = -1000;
  Move * myturn;
  Move * bestMove;
  for (unsigned i = 0; i < available.size(); i++)
	{
		myturn = available[i];
		if (getValue(myturn, side) > score)
		{
			score = getValue(myturn, side);
			bestMove = myturn;
		}
		
	}
	return bestMove;
}
/*
 * fills board available moves vector with all of the available moves. 
 */
 void Board::get_moves(Side side)
 {
	 available.clear();
	 Move * myturn2;
	 for (int i = 0; i < 8; i++)
	 {
		 for (int j = 0; j < 8; j++)
		 {
			 myturn2 = new Move (i,j);
			 // adds any valid moves to vector or else, delete pointer.
			 if (checkMove(myturn2, side))
			 {
				//delete available[0];
				
				available.push_back(myturn2);
				
			 }
			 else
			 {
				 delete myturn2;
			 }
		 }
	 }
	 return;
		
 }

/*
 * Returns true if a move is legal for the given side; false otherwise.
 */
bool Board::checkMove(Move *m, Side side) {
    // Passing is only legal if you have no moves.
    if (m == NULL) return !hasMoves(side);

    int X = m->getX();
    int Y = m->getY();

    // Make sure the square hasn't already been taken.
    if (occupied(X, Y)) return false;

    Side other = (side == BLACK) ? WHITE : BLACK;
    for (int dx = -1; dx <= 1; dx++) {
        for (int dy = -1; dy <= 1; dy++) {
            if (dy == 0 && dx == 0) continue;

            // Is there a capture in that direction?
            int x = X + dx;
            int y = Y + dy;
            if (onBoard(x, y) && get(other, x, y)) {
                do {
                    x += dx;
                    y += dy;
                } while (onBoard(x, y) && get(other, x, y));

                if (onBoard(x, y) && get(side, x, y)) return true;
            }
        }
    }
    return false;
}
/*
 * checks to see if a move is a stable move, that is the move can't
 * be taken back.
 */
bool Board::stable_move(Move * move, Side side)
{
	// base case: corner is always stable
	if (getValue(move, side) == CORNERSCORE)
	{
		return true;
	}
	// an unoccupied space along edge for both sides means stable
	if (! occupied(move->getX(), move->getY()))
	{
		return true;
	}
	bool result;
	Move * moveA;
	Move * moveB;
	// check for stability along edge of move for both sides.
	if (move->getX() == 0)
	{
		moveA = new Move(move->getX(), move->getY()+1);
		moveB = new Move(move->getX(), move->getY()-1);
	}
	else if(move->getX() == 7)
	{
		moveA = new Move(move->getX(), move->getY()+1);
		moveB = new Move(move->getX(), move->getY()-1);
	}
	else if(move->getY() == 0)
	{
		moveA = new Move(move->getX()+1, move->getY());
		moveB = new Move(move->getX()-1, move->getY());
	}
	else if (move->getY() == 7)
	{
		moveA = new Move(move->getX()+1, move->getY());
		moveB = new Move(move->getX()-1, move->getY());
	}
	else 
	{
		// case where not along edge,
		return false;
	}
	result = (stable_move(moveA, side) && stable_move(moveB,side));
	delete moveA;
	delete moveB;
	return result;
}
/*
 * Modifies the board to reflect the specified move.
 */
void Board::doMove(Move *m, Side side) {
    // A NULL move means pass.
    if (m == NULL) return;

    // Ignore if move is invalid.
    if (!checkMove(m, side)) return;

    int X = m->getX();
    int Y = m->getY();
    Side other = (side == BLACK) ? WHITE : BLACK;
    for (int dx = -1; dx <= 1; dx++) {
        for (int dy = -1; dy <= 1; dy++) {
            if (dy == 0 && dx == 0) continue;

            int x = X;
            int y = Y;
            do {
                x += dx;
                y += dy;
            } while (onBoard(x, y) && get(other, x, y));

            if (onBoard(x, y) && get(side, x, y)) {
                x = X;
                y = Y;
                x += dx;
                y += dy;
                while (onBoard(x, y) && get(other, x, y)) {
                    set(side, x, y);
                    x += dx;
                    y += dy;
                }
            }
        }
    }
    set(side, X, Y);
}

/*
 * Current count of given side's stones.
 */
int Board::count(Side side) {
    return (side == BLACK) ? countBlack() : countWhite();
}

/*
 * Current count of black stones.
 */
int Board::countBlack() {
    return black.count();
}

/*
 * Current count of white stones.
 */
int Board::countWhite() {
    return taken.count() - black.count();
}
/*
 * Returns score of current board
 */
 
 int Board::getScore(Side score_side)
 {

	 if (score_side == BLACK)
	 {
		 return countBlack() - countWhite();
	 }
	 return countWhite() - countBlack();
 }
 
/*
 * Returns weighted score after a move
 */
 
 int Board::getScore_weighted(Move * move, Side move_side, Side score_side,
  Board * board)
 {
	 int my_score = 0;
	 int their_score = 0;
	 for (int i = 0; i < 8; i++)
	{
		for (int j = 0; j < 8; j++)
		{
			
		}
		
	}
	return my_score - their_score;
 }
/*
 * Returns the value of the move
 */
 int Board::getValue(Move * move, Side side)
 {
	
	 if (move->getX() == 0 && move->getY() == 0)
	 {
		 return CORNERSCORE;
	 }
	 if (move->getX() == 7 && move->getY() == 7)
	 {
		 return CORNERSCORE;
	 }
	 if (move->getX() == 0 && move->getY() == 7)
	 {
		 return CORNERSCORE;
	 }
	 if (move->getX() == 7 && move->getY() == 0)
	 {
		 return CORNERSCORE;
	 }
	 if (move->getX() == 1 && move->getY() == 1)
	 {
		 return CROSSCORNER;
	 }
	 if (move->getX() == 1 && move->getY() == 6)
	 {
		 return CROSSCORNER;
	 }
	 if (move->getX() == 6 && move->getY() == 6)
	 {
		 return CROSSCORNER;
	 }
	 if (move->getX() == 6 && move->getY() == 1)
	 {
		 return CROSSCORNER;
	 }
	 if (move->getX() == 0 && (move->getY() == 6 || move->getY() == 1))
	 {
		 return NEARCORNER;
	 }
	 if (move->getX() == 7 && (move->getY() == 6 || move->getY() == 1))
	 {
		 return NEARCORNER;
	 }
	 if (move->getX() == 1 && (move->getY() == 0 || move->getY() == 7))
	 {
		 return NEARCORNER;
	 }
	 if (move->getX() == 6 && (move->getY() == 0 || move->getY() == 7))
	 {
		 return NEARCORNER;
	 }
	 if (move->getX() == 0 || move->getY() == 0 || move->getY() == 7 ||
	     move->getX() == 7)
	 {
		 return ENDSCORE;
	 }
	 if (move->getX() == 1 || move->getY() == 1 || move->getY() == 6 ||
	     move->getX() == 6)
	 {
		 return NEARENDSCORE;
	 }
	 return 1;
 }

/*
 * Sets the board state given an 8x8 char array where 'w' indicates a white
 * piece and 'b' indicates a black piece. Mainly for testing purposes.
 */
void Board::setBoard(char data[]) {
    taken.reset();
    black.reset();
    for (int i = 0; i < 64; i++) {
        if (data[i] == 'b') {
            taken.set(i);
            black.set(i);
        } if (data[i] == 'w') {
            taken.set(i);
        }
    }
}
